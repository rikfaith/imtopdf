#!/usr/bin/env python3
# main.py -*-python-*-

import argparse
import json

import imtopdf.image
import imtopdf.pad

# pylint: disable=unused-import
from imtopdf.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE

def main():
    parser = argparse.ArgumentParser(description='imtopdf')
    parser.add_argument('files', nargs='*', help='Life if files to process')
    parser.add_argument('--info', action='store_true', default=False,
                        help='Display information about the files')
    parser.add_argument('--clean', action='store_true', default=False,
                        help='Clean image')
    parser.add_argument('--pad', action='store_true', default=False,
                        help='Pad all images with white board')
    parser.add_argument('--version', action='store_true', default=False,
                        help='Display version')
    parser.add_argument('--debug', action='store_true', default=False,
                        help='Increase logging verbosity')
    parser.add_argument('--nodebug', nargs='+', metavar='MODULE',
                        help='Modules for which debugging is disabled')
    args = parser.parse_args()

    if args.debug:
        LOG_SET_LEVEL('DEBUG')
        if args.nodebug:
            for module in args.nodebug:
                LOG_SET_LEVEL('INFO', module=module)

    if args.version:
        INFO(f'imtopdf version {imtopdf.__version__}')
        return 0

    if len(args.files) < 1 or (not args.clean and not args.pad and \
                               not args.info):
        parser.print_help()
        return -1

    if args.pad:
        pad = imtopdf.pad.Pad(args.files, debug=args.debug)
        pad.analyze()
        pad.pad()
        return 0

    for file in args.files:
        image = imtopdf.image.Image(file, debug=args.debug)
        if args.info:
            result = image.info()
            print(json.dumps(result, indent=4, sort_keys=False))
        if args.clean:
            image.clean()

    return 0
