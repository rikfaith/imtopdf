#!/usr/bin/env python3
# info.py -*-python-*-

from matplotlib import pyplot as plt
import os

import cv2
import numpy as np
import PIL
import tesserocr

# pylint: disable=unused-import
from imtopdf.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE


class Image():
    def __init__(self, filename, debug=False):
        self.filename = filename
        self.debug = debug
        self.image = cv2.imread(filename, cv2.IMREAD_UNCHANGED)
        self.gray = cv2.imread(filename, cv2.IMREAD_GRAYSCALE)
        INFO(f'{filename=}')

    def _filename(self, original, prefix, fmt):
        dirname, basename = os.path.split(original)
        root, ext = os.path.splitext(basename)
        return os.path.join(dirname, prefix + root + '.' + fmt)

    def info(self):
        result = {}
        result['filename'] = self.filename
        result['height'] = self.image.shape[0]
        result['width'] = self.image.shape[1]
        if len(self.image.shape) > 2:
            result['channels'] = self.image.shape[2]
        result['type'] = str(self.image.dtype)
        return result

    def _write_message(self, image, message):
        xpos = 50
        ypos = 50
        image = image.copy()

        # Write the message onto the image.
        (text_w, text_h), _ = cv2.getTextSize(message,
                                              cv2.FONT_HERSHEY_SIMPLEX,
                                              1,
                                              1)
        cv2.rectangle(image, (xpos, ypos), (xpos + text_w, ypos + text_h),
                      (0, 0, 0), -1)
        cv2.putText(image, message, (xpos, ypos + text_h),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 1,
                    cv2.LINE_AA)
        return image


    def _show(self, name, image, xpos=1200, ypos=0):
        if self.debug:
            image = self._write_message(image, name)
            cv2.namedWindow(name)
            cv2.moveWindow(name, xpos, ypos)
            cv2.imshow(name, image)
            cv2.waitKey()
            cv2.destroyAllWindows()

    def scale(self, image, max_dim=1600):
        height, width = image.shape[:2]
        if max(width, height) <= max_dim:
            return image.copy()

        scale = max_dim / max(width, height)
        width = int(width * scale)
        height = int(height * scale)

        if scale > 1.0:
            return image

        return cv2.resize(image, (width, height), interpolation=cv2.INTER_AREA)

    def distances_to_edge(self, point, shape):
        lines = [
            [np.array([0, 0]), np.array([0, shape[0]])], # left
            [np.array([0, 0]), np.array([shape[1], 0])], # top
            [np.array([shape[1], shape[0]]), np.array([0, shape[0]])], # bottom
            [np.array([shape[1], shape[0]]), np.array([shape[1], 0])]] # right

        distances = []
        for line in lines:
            distance = np.abs(
                np.cross(line[1] - line[0], line[0] - point) /
                np.linalg.norm(line[1] - line[0]))
            distances.append(min(distance))
        return distances

    def rotate(self, image, skew, center=None):
        height, width = image.shape[:2]
        if center is None:
            # Use image center
            center = (width // 2, height // 2)
        rotmat = cv2.getRotationMatrix2D(center, skew, 1.0)
        rotated = cv2.warpAffine(image, rotmat, (width, height),
                                 flags=cv2.INTER_CUBIC,
                                 borderMode=cv2.BORDER_CONSTANT,
                                 borderValue=(255, 255, 255))
        return rotated

    def shift(self, image, xoffset, yoffset):
        height, width = image.shape[:2]
        transmat = np.float32([ [1, 0, xoffset],
                                [0, 1, yoffset] ])
        shifted = cv2.warpAffine(image, transmat, (width, height),
                                 borderMode=cv2.BORDER_CONSTANT,
                                 borderValue=(255, 255, 255))
        return shifted


    def fill(self, mask, image):
        INFO(f'{mask.shape=} {image.shape=}')
        self._show('fill: image', image)
        self._show('fill: mask', mask)
        filled = cv2.floodFill(image, mask, (1, 1), 255)
        self._show('fill: filled', filled)
        return filled

    def blur(self, image, border=10, size=9):
        # Blur the image
        blur = cv2.GaussianBlur(image, (size, size), 0)

        if border > 0:
            # Add a black boarder around the edges.
            blur = cv2.rectangle(blur, (0, 0),
                                 (blur.shape[1], blur.shape[0]),
                                 (0, 0, 0), border)

        # Threshold.
        _, blur = cv2.threshold(blur, 0, 255, cv2.THRESH_OTSU)

        return blur

    def crop(self, image, blur, shave=5, size=5):
        # The first and second threshold parameters are selected arbitrarily
        # from examples.
        edges = cv2.Canny(np.asarray(blur), 100, 200)

        # Perform dilation.
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (size, size))
        edges = cv2.dilate(edges, kernel, iterations=1)

        # Find and order the contours.
        contours, _ = cv2.findContours(edges, cv2.RETR_LIST,
                                       cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)

        # Crop the original input image using the largest rectangle.
        xpos, ypos, width, height = cv2.boundingRect(contours[0])
        image = image[ypos + shave : ypos + height - 2 * shave,
                      xpos + shave : xpos + width - 2 * shave].copy()

        return image

    def locate_text(self, blur, size=10):
        # Perform dilation. Use a larger kernel than for cropping, because the
        # goal is to merge all the text into a few large blobs.
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (size, size))
        blur = cv2.dilate(cv2.bitwise_not(blur), kernel, iterations=3)

        # Find and order the contours.
        contours, _ = cv2.findContours(blur, cv2.RETR_LIST,
                                       cv2.CHAIN_APPROX_SIMPLE)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)

        if self.debug:
            tmp = cv2.cvtColor(blur, cv2.COLOR_GRAY2RGB)
            tmp = cv2.drawContours(tmp, contours, -1, (255, 0, 0), 3)
            self._show('contours', tmp)

        # Compute a mask that covers all of the text regions. We skip regions
        # that are too small.
        mask = np.zeros(blur.shape, dtype=np.uint8)
        angles = []
        areas = []
        distances = []
        min_distance = 10
        while len(areas) == 0 and min_distance >= 0:
            for contour in contours:
                area = cv2.contourArea(contour)
                rect = cv2.minAreaRect(contour)
                angle = rect[-1]
                points = np.int0(cv2.boxPoints(rect))
                dist = self.distances_to_edge(points, blur.shape)

                if area < 1600:
                    # Skip areas that are too small.
                    continue

                INFO(f'{area=} {dist=} {angle=}')
                cv2.fillPoly(mask, [points], [255, 255, 255])
                areas.append(area)
                distances.append(dist)
                angles.append(angle)
            min_distance -= 2

        if len(areas) == 0:
            FATAL('No text found. Blank page?')
            mask = np.ones(blur.shape, dtype=np.uint8)
        self._show('mask', mask)
        return mask, areas, distances, angles

    def upside_down(self, image, areas, distances):
        with tesserocr.PyTessBaseAPI(psm=tesserocr.PSM.AUTO_OSD) as api:
            img = PIL.Image.fromarray(image)
            api.SetImage(img)
            api.Recognize()
            layout = api.AnalyseLayout()
            if layout is None:
                # There is no text, so it's unclear what to do here.
                yoffsets = []
                for distance in distances:
                    yoffsets.append(int(-(distance[1] - distance[2]) // 2))
                INFO(f'{yoffsets=}')
                offset = max(yoffsets, key=abs)
                if offset > 0:
                    return True
                return False

            orientation, direction, order, deskew_angle = layout.Orientation()
            INFO(f'{orientation=} {direction=} {order=} {deskew_angle=:.2f}')
            if orientation == tesserocr.Orientation.PAGE_DOWN:
                return True
        return False

    def find_offset(self, areas, distances, shape):
        # As a first approximation, use the first contour, but this might be
        # incorrect for a title page. So, find all of the distances, and use
        # the smallest correction.
        xoffsets = []
        max_area = 0
        for area, distance in zip(areas, distances):
            max_area = max(area, max_area)
            if area < max_area / 10:
                # Skip the smaller areas.
                continue
            xoffsets.append(int(-(distance[0] - distance[3]) // 2))

        INFO(f'{xoffsets=} {shape=}')

        offset = min(xoffsets, key=abs)
        if len(distances) == 1 and abs(offset) > shape[1] // 4:
            offset = 0
            INFO('NOT centering because single contour is already offset')

        return offset

    def find_skew(self, areas, distances, angles, shape):
        results = [] # skew, (xcenter, ycenter), euclid
        max_area = 0
        for area, distance, angle in zip(areas, distances, angles):
            max_area = max(area, max_area)
            if area < max_area / 10:
                # We want to compute the skew based on the large-ish text
                # areas. For title pages, there may be more than one, but we
                # don't care about the smaller areas.
                continue
            skew = angle
            if abs(angle - 90) < 10:
                skew = angle - 90
            center = (int((distance[0] + shape[1] - distance[3]) // 2),
                      int((distance[1] + shape[0] - distance[2]) // 2))
            euclid = np.linalg.norm(np.array(center) - shape[:2])
            INFO(f'{area=} {skew=:.2f} {center=} {euclid=:.2f}')
            results.append((skew, center, euclid))

        results.sort(key=lambda tup: abs(tup[0]))
        return results[0][:2]

    def trunc(self, image, multiplier=1.0):
        # Change upper non-white pixels to white, but leave darker pixels
        # unchanged. For the human reader, we want to preserve the grayscale
        # around the letters for improved readability.
        mean = np.mean(image)
        value = int(mean * multiplier)
        target = np.where((image >= value))
        INFO(f'{mean=:.1f} {value=}')
        image[target] = 255

        return image

    def prep(self, image, max_dim):
        # If the image is large, get a scaled version.
        image = self.scale(image, max_dim=max_dim)
        image = self.trunc(image, multiplier=1.1)
        return image

    def clean(self, prefix='tmp-', fmt='png', border=10,
              max_dims=[1600, 1200]):
        image = self.prep(self.gray, max_dims[0])
        self._show('start', image)

        # Get a blurred version of the image for processing.
        blur = self.blur(image, border=10)
        self._show('blur 1', blur)

        # Crop the image to get rid of overscanned borders.
        image = self.crop(image, blur, size=5, shave=10)
        self._show('cropped', image)

        # Get a new blurred image after the cropping operation.
        blur = self.blur(image, border=0, size=7)
        self._show('blur 2', blur)

        # Determine a mask and rectangles that cover the text.
        mask, areas, distances, angles = self.locate_text(blur, size=9)

        # Fill in the areas outside the mask.
        imask = cv2.bitwise_not(mask)
        image = cv2.bitwise_or(image, image, mask=mask)
        image = cv2.bitwise_or(image, imask)
        self._show('filled', image)

        # Determine the skew, along with the center of the rectangle in the
        # coordinates of the image.
        skew, center = self.find_skew(areas, distances, angles, image.shape)
        if abs(skew) < 10 and abs(skew > 0.1):
            INFO(f'deskewing: {skew=:.2f} {center=}')
            image = self.rotate(image, skew, center)
            self._show(f'rotated {skew=:.2f}', image)

        # Try to center the main body of text.
        xoffset = self.find_offset(areas, distances, image.shape)
        if abs(xoffset) > 0:
            INFO(f'centering: {xoffset=}')
            image = self.shift(image, xoffset, 0)
            self._show(f'shift {xoffset=}', image)

        # Reduce the resolution before using tesseract.
        image = self.scale(image, max_dim=max_dims[1])

        # Flip the image if likely it is upside down.
        if self.upside_down(image, areas, distances):
            INFO('inverting image')
            image = self.rotate(image, 180)

        image = self.trunc(image, multiplier=0.9)

        self._show('final', image)
        filename = self._filename(self.filename, prefix, fmt)
        cv2.imwrite(filename, image)
        return image
